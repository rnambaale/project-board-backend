# project board

Backend for the project board

## Prerequisites

The following tools will be needed to run this application successfully:

- Node v10.15.0 or above
- Npm v6.4 or above
- Postgres 9.6.14 or above

## Getting Started

**On your Local Machine**

- `git clone https://bitbucket.org/rnambaale/project-board-backend.git`
- Run `npm install` to install all dependencies
- Run npm start to start the app
- Access endpoints on **localhost:5000**
